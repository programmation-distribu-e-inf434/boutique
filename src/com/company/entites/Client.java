package com.company.entites;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

public class Client extends Personne {
    private String cin, carteVisa;
    private ArrayList<Achat> achats;

    public Client(long id, String nom, String prenom, LocalDateTime dateNaissance, String cin, String carteVisa) {
        super(id, nom, prenom, dateNaissance);
        this.carteVisa = carteVisa;
        this.cin = cin;
    }

    // todo getters

    public String getCin() {
        return cin;
    }

    public String getCarteVisa() {
        return carteVisa;
    }

    public ArrayList<Achat> getAchats() {
        return achats;
    }

    // todo setters

    public void setCin(String cin) {
        this.cin = cin;
    }

    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }

    public void setAchats(ArrayList<Achat> achats) {
        this.achats = achats;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), cin, carteVisa);
    }
}
