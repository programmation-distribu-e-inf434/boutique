package com.company.entites;

public class ProduitAchete {

    private int quantite;
    private double remise;
    private Produit produit;

    public ProduitAchete(int quantite, Double remise, Produit produit) {
        this.quantite = quantite;
        this.produit = produit;
        if (remise != null)
            this.remise = remise;
        else this.remise = 0d;
    }

    // todo getters

    public int getQuantite() {
        return quantite;
    }

    public double getRemise() {
        return remise;
    }

    public Produit getProduit() {
        return produit;
    }

    public double getPrixTotal() {
        return produit.getPrixUnitaire() *quantite - remise;
    }

    // todo setters

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }
}
