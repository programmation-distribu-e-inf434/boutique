package com.company.entites;

import java.time.LocalDateTime;

public class Produit {
    private final long id;
    private String libelle;
    private double prixUnitaire;
    private LocalDateTime datePeremption;
    private Categorie categorie;

    public Produit(long id, String libelle, double prixUnitaire, LocalDateTime datePeremption, Categorie categorie) {
        this.id = id;
        this.libelle = libelle;
        this.prixUnitaire = prixUnitaire;
        this.datePeremption = datePeremption;
        this.categorie = categorie;
    }

    // todo getters

    public long getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public double getPrixUnitaire() {
        return prixUnitaire;
    }

    public LocalDateTime getDatePeremption() {
        return datePeremption;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    // todo setters

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public void setDatePeremption(LocalDateTime datePeremption) {
        this.datePeremption = datePeremption;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public boolean estPerime() {
        return estPerime(LocalDateTime.now());
    }

    public boolean estPerime(LocalDateTime dateReference) {
        return dateReference.compareTo(datePeremption) < 0;
    }

}
