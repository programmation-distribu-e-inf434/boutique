package com.company.entites;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Achat {

    private final long id;
    private LocalDateTime dateAchat;
    private double remise;
    private ArrayList<ProduitAchete> produitsAchetes;

    public Achat(long id, LocalDateTime dateAchat, Double remise) {
        this.id = id;
        this.dateAchat = dateAchat;
        if (remise != null)
            this.remise = remise;
        else this.remise = 0d;
    }

    // todo getters

    public long getId() {
        return id;
    }

    public LocalDateTime getDateAchat() {
        return dateAchat;
    }

    public double getRemise() {
        return remise;
    }

    public double getRemiseTotale() {
        return 0;
    }

    public double getPrixTotal() {
        return 0;
    }

    // todo setters

    public void setDateAchat(LocalDateTime dateAchat) {
        this.dateAchat = dateAchat;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }
}
