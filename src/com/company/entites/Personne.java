package com.company.entites;

import java.time.LocalDateTime;
import java.util.Objects;

public class Personne {

    protected final long id;
    protected String nom, prenom;
    protected LocalDateTime dateNaissance;

    public Personne(long id, String nom, String prenom, LocalDateTime dateNaissance) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Personne)
            return id == ((Personne) other).id;
        else return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, dateNaissance);
    }

    @Override
    public String toString() {
        return "Personne{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance=" + dateNaissance +
                '}';
    }

    // todo getters

    public long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public LocalDateTime getDateNaissance() {
        return dateNaissance;
    }

    int getAge() {
        return getAge(LocalDateTime.now());
    }

    int getAge(LocalDateTime dateReference) {
        return dateReference.getYear() -dateNaissance.getYear();
    }

    // todo setter

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setDateNaissance(LocalDateTime dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
}
