package com.company.entites;

import java.util.Objects;

public class Categorie {
    private final int id;
    private String label, description;

    public Categorie(int id, String label, String description) {
        this.id = id;
        this.label = label;
        this.description = description;
    }

    // todo getters

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    // todo setters

    public void setLabel(String label) {
        this.label = label;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Categorie)
            return id == ((Categorie) other).id;
        else return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, label, description);
    }
}
