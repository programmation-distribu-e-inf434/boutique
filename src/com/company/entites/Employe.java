package com.company.entites;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

public class Employe extends Personne {
    private String cnss;
    private ArrayList<Achat> achats;

    public Employe(long id, String nom, String prenom, LocalDateTime dateNaissance, String cnss) {
        super(id, nom, prenom, dateNaissance);
        this.cnss = cnss;
    }

    // todo getters

    public ArrayList<Achat> getAchats() {
        return achats;
    }

    public String getCnss() {
        return cnss;
    }

    // todo setters

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    public void setAchats(ArrayList<Achat> achats) {
        this.achats = achats;
    }

    @Override
    public String toString() {
        return "Employe{" +
                "id=" + id +
                ", cnss='" + cnss + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance=" + dateNaissance +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), cnss);
    }
}
